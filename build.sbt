name := "CS493"

scalaVersion := "2.10.4"


version := "1.0-SNAPSHOT"

// The Typesafe repository 
resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "org.reactivemongo" %% "play2-reactivemongo" % "0.10.2",
  "com.github.t3hnar" %% "scala-bcrypt" % "2.4"
)     

play.Project.playScalaSettings
