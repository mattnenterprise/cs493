package trade

import akka.actor.Actor
import Hermes._
import Jobs._
import controllers.Booter.everybody
import reactivemongo.api.MongoDriver
import reactivemongo.bson._
import reactivemongo.api.collections.default.BSONCollection
import scala.concurrent.ExecutionContext.Implicits.global
import controllers.Booter

class Brain(var body: Person) extends Actor{
  body.job match {
    case Bartering(offers)
    	=> offers.foreach( o => Market.allTraders.apply(o.what.id) ! o)
    case _ => Unit
  }
  
  
	def receive = {
	  case Ok(Cancel(x, who)) => {
	    body = body.updateAll
	    body = body.cancelOffer(x)
	    update
	    who ! Body(body)
	  }
	  case Cancel(x, who) => {
	    Market.allTraders.apply(x.what.id) ! Cancel(x, who)
	  }
	  case Failed(Cancel(x, who)) => {
	    update
	    who ! Body(body)
	  }
	  case ChangeJob(x) => {
	    var newBody = body.updateAll
	    try newBody = newBody.changeJob(Jobs.jobDictionary.apply(x))
	    catch {
	      case TooManyBids => sender ! Text("Must clear all market offers before changing jobs")
	    }
	    if (newBody != body){
	      body = newBody
	      update
	      
	    }
	    else sender ! Text("Already doing that thing")
	  }
	  case x : Deal => try {
	    body = body.updateAll
	    val(newBody, leftover) = body.resolveOffer(x.offer , x.amt, x.rate)
	    leftover match{
	      case Some(x) => sender ! x
	      case _ => Unit
	    }
	    body = newBody
	    update
	  } catch{
	    case _ : Throwable => sender ! Failed(x)
	  }
	  
	  case ExCancel(x) => {
	    Market.allTraders.apply(x.what.id) ! Cancel(x, sender)
	  }
	   
	  case x : Bid => try {
	    
	    body = body.updateAll
	    body = body.addOffer(x)
	    Market.allTraders.apply(x.what.id) ! x
	    sender ! Body(body)
	    update
	  } catch {
	    case NotOfferingThatMuch => sender ! Text("Not Enough Stuff")
	    case TooManyBids => sender ! Text("Too many bids")
	  }
	  case Refresh => {
	    var newbody = body.updateAll
	    if (newbody != body) { body = newbody; update}
	    sender ! Body(body)
	  }
	  case _ => println("Received something that we don't know")
	}
	private def update = {
	  everybody.update(BSONDocument("_id" -> body.id), body)
	}
}