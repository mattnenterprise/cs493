package trade;

import Good._
import Jobs._
import Hermes._
import java.util.Date
import reactivemongo.bson._
import play.modules.reactivemongo.json.BSONFormats._
import play.api.libs.json._
import scala.annotation.tailrec


case class Person (id: BSONObjectID, job: Job, times: (Long, Long), prestige: Long,  inv: Inventory) {
  
 
 import Person._
  
  def consumeFood(): Person = {
    val nextEaten = times._1 + FOOD_FREQ
    if (System.currentTimeMillis() > nextEaten){
      if (inv.get(food) > 0) new Person(id, job, (nextEaten, times._2), prestige + PRESTIGE_FOR_FOOD, inv.update(food, _-1)).consumeFood
      else new Person(id, job, (nextEaten, times._2), prestige, inv)
    }  
    else this
  }
  
  
  def consumeSupply(): Person = {
    val nextSupply = times._2 + SUPPLY_FREQ
    if (System.currentTimeMillis() > nextSupply)
      if (inv.get(supply) > 0) new Person(id, job, (times._1, nextSupply), prestige + PRESTIGE_FOR_SUPPLY, inv.update(supply, _-1)).consumeSupply
      else new Person(id, job, (times._1, nextSupply), prestige, inv)
    else this
  }
  
  
  def updateWork(): Person = {
    job match{
      case MyWork(startTime, task) if System.currentTimeMillis() > startTime + task.rate => 
          val newInv = inv.zip(task.gain)
          new Person(id, MyWork(startTime + task.rate, task), times, prestige, (if (newInv.valid) newInv else inv))
      case _ => this
    }
  }
  //Give the offer to market afterward!
  def addOffer(offer: Bid) : Person = {
    val newInv = 
        if (offer.isBuying)
          inv.updateGold( _- (offer.howMany * offer.rate))
        else
          inv.update(offer.what, _-offer.howMany)
    if (! newInv.valid) throw NotOfferingThatMuch
    val newJob = job match{
      case x : Bartering => x.add(offer)
      case _ => new Bartering(List(offer))
    }
    new Person(id, newJob, times, prestige, newInv)
  }
  //Only market should call this!
  def resolveOffer(offer: Bid, amt: Int, actualPrice : Int): (Person, Option[Bid]) = {
    if (! job.isInstanceOf[Bartering]) throw OfferNotFound
    
    var newJob = job.asInstanceOf[Bartering].remove(offer)
    val option = {
      val leftOver = offer.howMany - amt
      if (leftOver > 0){
        val bid = Bid(offer.isBuying, offer.what, leftOver, offer.rate)
        newJob = newJob.add(bid)
        Some(bid)
      } 
      else None
    }
    val newInv =
      if (offer.isBuying)
        inv.update(offer.what, _+amt).updateGold(_+((offer.rate - actualPrice) * amt))
      else
        inv.updateGold(_+ (offer.howMany * actualPrice))
    (new Person(id, newJob, times, prestige, newInv), option)
      
  }
  
  //Remove from market first!
  def cancelOffer(offer: Bid) : Person = {
    val newJob = job match{
      case x : Bartering => x.remove(offer)
      case _ => throw OfferNotFound
    }
    val newInv = 
        if (offer.isBuying)
          inv.updateGold( _+ (offer.howMany * offer.rate))
        else
          inv.update(offer.what, _+offer.howMany)
    new Person(id, newJob, times, prestige, newInv)
  }
  
  def changeJob(newJob: Work) = job match{
      case Bartering(List()) | MyWork(_, _) => new Person(id, MyWork(System.currentTimeMillis(), newJob), times, prestige, inv)
      case _ : Bartering => throw TooManyBids
    }
  def updateAll : Person = {
    var newPerson  = this
    def helper(start: Person, func : (Person => Person)) =  {
      var nPerson = start
      var nrPerson = func(nPerson)
      while (nPerson != nrPerson) { nPerson = nrPerson; nrPerson = func(nPerson)}
      nrPerson
    }
    val worker = helper(this, _.updateWork)
    val feaster = helper(worker, _.consumeFood)
    helper(feaster, _.consumeSupply)
    
  }
  
  override def toString() = {
    "ID: " + id + 
    "\nJob: " + job +
    "\nLast Eaten: " + new Date(times._1)
    "\nLast Supplied: " + new Date(times._2)
    "\nGold: " + inv.gold +
    "\nPrestige: " + prestige
    "\nInventory: " + inv.toString
  }
}






object Person{
  val PRESTIGE_FOR_FOOD = 3
  val PRESTIGE_FOR_SUPPLY = 10
  val PRESTIGE_FOR_IDLING = 1
  val GOLD_FOR_MINTING = 15
  val FOOD_FREQ = 1000 * 180 * 1
  val SUPPLY_FREQ = 1000 * 180 * 3
  
  def makeNew( id : BSONObjectID) : Person = {
    val now = System.currentTimeMillis()
    Person(id, Jobs.MyWork(now, Jobs.Idling), (now, now), 0L, Inventory.empty.updateGold(_+30))
  }
  
import play.api.libs.json._

val PersonWrites = new Writes[Person] {
  def writes(foo: Person) = Json.obj(
    "_id" -> foo.id,
	"job" -> Jobs.JsJobWrites.writes(foo.job), //Has it's own converter
	"ftime" -> ((foo.times._1 +  Person.FOOD_FREQ - System.currentTimeMillis()) / 1000),
	"stime" -> ((foo.times._2 + Person.SUPPLY_FREQ - System.currentTimeMillis()) / 1000),
	"prestige" -> foo.prestige,
	"inv" -> Inventory.JsInvWrites.writes(foo.inv)
  )
}
  
  
  implicit object PersonBSONWriter extends BSONDocumentWriter[Person] {
    
		def write(foo: Person): BSONDocument =
				BSONDocument(
						"_id" -> foo.id,
						"job" -> foo.job, //Has it's own converter
						"ftime" -> foo.times._1,
						"stime" -> foo.times._2,
						"prestige" -> foo.prestige,
						"inv" -> foo.inv) 
	}

	implicit object PersonBSONReader extends BSONDocumentReader[Person] {
		def read(document: BSONDocument): Person =
				Person(
						document.getAs[BSONObjectID]("_id").get,
						document.getAs[Job]("job").get,
						(	document.getAs[Long]("ftime").get,
							document.getAs[Long]("stime").get
						),
						document.getAs[Long]("prestige").get,
						document.getAs[Inventory]("inv").get
						)
	}
}



