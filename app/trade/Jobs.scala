package trade

import Good._
import Hermes._
import Hermes.Bid._
import scala.collection.immutable.HashMap
import reactivemongo.bson._
import play.api.libs.json._
import play.modules.reactivemongo.json.BSONFormats._


case class Work (rate: Long, gain: Inventory, text: String)
sealed trait Job
object Jobs {
	val nothing = Vector.fill(Good.maxId)(0)
	private val stdTime = 1000 * 10
	
	
	
	case class MyWork(timeStamp: Long, work: Work) extends Job
	
	
	implicit object JobBSONWriter extends BSONDocumentWriter[Job] {
		def write(foo: Job): BSONDocument =
				foo match {
		  case MyWork(time, work)
		  	=> BSONDocument( "time" -> time, "name" -> nameDictionary.get(work).get)
		  case Bartering(offers) 
		  	=> BSONDocument( "offers" -> {
		  	  var base = BSONDocument.empty
		  	  for (index <- 0 until offers.length) base = base ++ BSONDocument(index.toString -> offers.apply(index))
		  	  base
		  	})
		  	 
		}
		
		//case class Bid( who: Long, isBuying: Boolean,  what: Good.Goods, howMany: Int, rate: Int)
	}
	
	implicit val JsJobWrites = new Writes[Job] {
  override def writes(foo: Job) = 
		  foo match {
		  case MyWork(time, work)
		  	=> Json.obj( "time" -> time, "name" -> nameDictionary.get(work).get)
		  case Bartering(offers) 
		  	=> Json.obj( "offers" -> JsArray(offers.toSeq.map(Json.toJson[Bid](_)(Hermes.Bid.JsFormatter))))
		  	   
		}
}

	implicit object JobBSONReader extends BSONDocumentReader[Job] {
		def read(document: BSONDocument): Job =
				document.getAs[Long]("time") match{
		  case Some(time) => MyWork(time, jobDictionary.get(document.getAs[String]("name").getOrElse("idle")).getOrElse(Idling))
		  case None => {
		    val offers = for (index <- 0 to 3; option = document.getAs[Bid](index.toString); if option.isDefined) yield option.get
		    Bartering(offers.toList)
		  }
		}
	}
			
	
	
	//TODO: figure out market/local record circle
	//Caller causes the person to remove bids, so maybe caller should be responsible for market?
	//Possibly pass back old and new orders along with Bartering for amalgams  
	//If error, returns the same.
	case class Bartering(offers: List[Bid]) extends Job{
	  
	  def add(foo: Bid): Bartering = 
	    offers.find(foo.isScalar) match{
	      case Some(x) => 
	        val index = offers.indexOf(x)
	        val compOffer = x + foo
	        Bartering(offers.updated(index, compOffer))
	      case None if offers.length < 4 =>
	        Bartering(foo :: offers)
	      case None =>
	        throw TooManyBids
	    
	  }
	  def remove(foo: Bid): Bartering =  
	    offers.find(foo.isScalar) match{
	    case Some(x) if x.howMany == foo.howMany =>
	      val (left, right) = offers.span(_!=x)
	      Bartering(left ++ (right.tail))
	    case Some(x) =>
	      val index = offers.indexOf(x)
	      val compOffer = x - foo
	      if (compOffer.howMany <= 0)
	        throw NotOfferingThatMuch
	      
	      Bartering(offers.updated(index, compOffer))
	    case None =>
	      throw OfferNotFound
	  }
	}
	    
	    
	   
	
	val jobDictionary = HashMap[String, Work](
	    "idle" -> Idling,
	    "farm" -> Farming,
	    "supply" -> Supplying,
	    "heard" -> Hearding,
	    "forest" -> Forestry,
	    "smith" -> Smithing,
	    "mine" -> Mining,
	    "pan" -> Panning,
	    "mint" -> Minting
	);
	
	lazy val nameDictionary : HashMap[Work, String] = jobDictionary.map(_.swap)
	
	
	object Idling extends Work(stdTime, 
	    Inventory.empty,
	    "You stand about idly.")
	object Farming extends Work(stdTime,
	    Inventory.empty.update(food, _+1),
	    "Your farming has paid off with a loaf of bread, ready to be eaten.")
	object Supplying extends Work(stdTime,
	    Inventory.empty.update(supply, _+2).update(cloth, _-1).update(tool, _-1),
	    "You package together supplies fit for a merchant.")
	object Hearding extends Work(stdTime,
	    Inventory.empty.update(cloth, _+1),
	    "Minding the sheep has earned you a roll of cloth.")
	object Forestry extends Work(stdTime,
	    Inventory.empty.update(wood , _+1),
	    "You return from the forest with an armload of wood.")
	object Smithing extends Work(stdTime,
	    Inventory.empty.update(wood, _-1).update(iron, _-1).update(tool, _+1),
	    "You hammer out a useful tool.")
	object Mining extends Work(stdTime,
	    Inventory.empty.update(iron, _+1),
	    "The mine yields iron ore, which you refine into a dull grey bar.")
	object Panning extends Work(stdTime * 3,
	    Inventory.empty.update(gBar, _+1),
	    "Finally! You've found a golden nugget!")
	object Minting extends Work(stdTime,
	    Inventory.empty.update(gBar, _-3).updateGold(_+30),
	    "You produce a pile of gold coins. The king takes his share, of course.")
	
	
}