angular.module("app")
	.controller("LoginCtrl", function($scope, $http, $location) {
		$scope.message="Please Login";
		$scope.invalidLogin = false;
		
		$scope.email = '';
		$scope.password = '';
		
		$scope.login = function() {
			$http.post('/api/user/login', JSON.stringify({email: $scope.email, password: $scope.password}))
				.success(function(data, status, headers, config) {
					$location.path('/game');
				})
				.error(function(data, status, headers, config) {
					$scope.invalidLogin = true;
				});
		};
	});