angular.module("app")
	.controller("SignupCtrl", function($scope, $http, $location) {
		$scope.email = '';
		$scope.password = '';
		$scope.emailInUse = false;
			
		$scope.signup = function() {
			$http.post('/api/user/create', JSON.stringify({email: $scope.email, password: $scope.password}))
				.success(function(data, status , headers, config) {
					if(data.status === "ERR") {
						$scope.emailInUse = true;
					} else {
						$location.path('/login');
					}
				}).
				error(function(data, status, headers, config) {
					$location.path('/main');
				});
		};
	});