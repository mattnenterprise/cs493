var app = angular.module("app", ["ngResource", "ngRoute", "ngSanitize", "ngWebsocket", "ui.bootstrap", "dialogs"])
	.config(["$routeProvider", function($routeProvider) {
		return $routeProvider.when("/", {
			templateUrl: "/views/main",
		}).when("/signup", {
			templateUrl: "/views/signup",
			controller: "SignupCtrl"
	    }).when("/login", {
	    	templateUrl: "/views/login",
	    	controller: "LoginCtrl"
	    }).when("/game", {
	    	templateUrl: "/views/game",
	    	controller: "GameCtrl"
	    }).otherwise({
			redirectTo: "/"
		});
	}
	]).config([
	"$locationProvider", function($locationProvider) {
		return $locationProvider.html5Mode(true).hashPrefix("!"); // enable the new HTML5 routing and history API
	}
	]);