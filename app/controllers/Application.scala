package controllers

import play.api._
import play.api.mvc._
import play.api.Play.current
import java.io.File

object Application extends Controller {

  def index(any: String) = Action {
    Ok(views.html.index())
  }
  
  def getURI(any: String): String = any match {
    case "main" => "/public/html/main.html"
    case "signup" => "/public/html/signup.html"
    case "login" => "/public/html/login.html"
    case "game" => "/public/html/game.html"
    case "changejob" => "/public/html/changejob.html"
    case "maketrade" => "/public/html/maketrade.html"
    case _ => "error"
  } 
  
  def loadAngularView(any: String) = Action {
    val projectRoot = Play.application.path
    val file = new File(projectRoot + getURI(any))
    if(file.exists()) {
    	Ok(scala.io.Source.fromFile(file.getCanonicalPath()).mkString).as("text/html")
    } else
    	NotFound
  }
}