package trade

import akka.actor.ActorRef
import play.api.libs.json.Format
import play.api.libs.json.JsError
import play.api.libs.json.JsResult
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import play.api.libs.json.Reads
import play.api.libs.json.Writes
import reactivemongo.bson.BSONBooleanHandler
import reactivemongo.bson.BSONDocument
import reactivemongo.bson.BSONDocumentReader
import reactivemongo.bson.BSONDocumentWriter
import reactivemongo.bson.BSONIntegerHandler
import reactivemongo.bson.Producer.nameValue2Producer

object Hermes {
  sealed trait Message
  
  //Messages that need Json values
  sealed trait BsMessage extends Message
  
  object Refresh extends BsMessage
  case class Body(person : Person) extends BsMessage
  case class Text(msg: String) extends BsMessage
  case class ExCancel(offer: Bid) extends BsMessage
  case class Cancel(offer: Bid, req: ActorRef) extends Message
  case class Ok( message :Message) extends Message
  case class Failed( message :Message) extends Message
  case class Reject( message :Message) extends Message
  case class ChangeJob(jobname: String) extends BsMessage
  
  implicit val MessageJsWriter = new Writes[BsMessage] {
    def writes(foo: BsMessage) = {
		foo match{
			case Text(x) => Json.obj(
				 "event" -> "Text", "data" -> x)
			case Body(x) => Json.obj(
				 "event" -> "Body", "data" -> Json.toJson(x)(Person.PersonWrites));
		}
    }
  }
  
  implicit val MessageJsReader = new Reads[BsMessage] {
    import Hermes.Bid._
    override def reads(document: JsValue) : JsResult[BsMessage] = {
		document.\("event").asOpt[String] match{
		  case Some("Cancel") => {
		    JsSuccess(ExCancel(document.\("data").as[Bid](JsFormatter)))
		  }
		  case Some("Bid") => JsSuccess(document.\("data").as[Bid](JsFormatter))
		  case Some("ChangeJob") => JsSuccess(ChangeJob(document.\("data").as[String]))
		  case Some("Refresh") => JsSuccess(Refresh)
		  case _ => JsError(Seq())
		  
		}
    }
  }

	
  
  
  case class Deal(offer: Bid, rate : Int, amt :Int) extends Message
  
  object Bid{
   
   implicit object JsFormatter  extends Format[Bid] {
     override def writes(foo: Bid) =  { 
		  Json.obj( "isBuying" -> foo.isBuying,
		  	        "what" -> foo.what.id,
		  	        "howMany" -> foo.howMany,
		  	        "rate" -> foo.rate )
		}
     override def reads(foo: JsValue) : JsResult[Bid] = {
       JsSuccess(Bid(foo.\("isBuying").as[Boolean], Good.apply(foo.\("what").as[Int]), foo.\("howMany").as[Int], foo.\("rate").as[Int])
     )}
   }
    
   implicit object BidWriter extends BSONDocumentWriter[Bid] {
		override def write(foo: Bid): BSONDocument =
				BSONDocument(
		  	        "isBuying" -> foo.isBuying,
		  	        "what" -> foo.what.id,
		  	        "howMany" -> foo.howMany,
		  	        "rate" -> foo.rate 
		  	        )
		}
	
  
  	implicit object BidReader extends BSONDocumentReader[Bid] {
		def read(document: BSONDocument): Bid =
			Bid( 
		           document.getAs[Boolean]("isBuying").get,
		           Good.apply(document.getAs[Int]("what").get),
		           document.getAs[Int]("howMany").get,
		           document.getAs[Int]("rate").get
		     )
		   
		
	}
    
  }
	case class Bid( isBuying: Boolean,  what: Good.Goods, howMany: Int, rate: Int) extends Comparable[Bid] with BsMessage{
	  def isSimilar(that: Bid) = {
	    (isBuying == that.isBuying) &&  what == that.what
	  }
	  def isScalar(that: Bid) = {
	    rate == that.rate && isSimilar(that)
	  }
	  override def compareTo(that: Bid) : Int = {
	    //Needlessly complicated, but safe. 
	    //Really only intended to put
	    //sells in least to greatest
	    //and buys in greatest to least
	    if (! isSimilar(that)){
	      val goodComp = what.compare(that.what)
	      if (goodComp != 0) goodComp
	      else isBuying.compare(that.isBuying)
	    }
	    if (isBuying)rate.compare(that.rate)
	    else that.rate.compare(rate)
	  }
	  def +(that: Bid) = {
	    assert(isScalar(that))
	    Bid(isBuying, what, howMany + that.howMany, rate)
	  }
	  def -(that: Bid) = {
	    assert(isScalar(that))
	    Bid(isBuying, what, howMany - that.howMany, rate)
	  }
	}

	  
	
		
}