package controllers
import akka.actor.{ActorRef, ActorSystem, Props, Inbox}
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.api.MongoDriver
import reactivemongo.bson._
import reactivemongo.api.collections.default.BSONCollection
import play.api.libs.iteratee.Iteratee
import scala.collection.mutable.HashMap
import play.api.mvc.WebSocket
import play.api.libs.iteratee.Enumerator
import scala.concurrent.duration._
import play.api.libs.json._
import play.api.mvc.Controller
import trade.Hermes
import trade.Person
import trade._
import trade.Hermes._

object Booter extends Controller {
	val system = ActorSystem("bizaar")
	
	val driver = new MongoDriver(system)
	
	val connection = driver.connection(List("localhost")) //Maybe not right?
	
	val db = connection.db("cs493") //Need to set up initial data base?
	
	
	val everybodyHash = new HashMap[BSONObjectID, ActorRef]
	
	val everybody = db[BSONCollection]("chars")
	val enum = everybody.find(BSONDocument()).cursor[BSONDocument].enumerate()
	
	val process : Iteratee[BSONDocument, Unit] =
  Iteratee.foreach { doc =>
    val num = doc.getAs[BSONObjectID]("_id").get
    everybodyHash+=((num, system.actorOf(Props(classOf[Brain], Person.PersonBSONReader.read(doc)))));
  }
	enum.run(process)
	//Needs to change to Json
		
	def talk = WebSocket.using[JsValue] { request => 
	  	//request needs to fish out a character ID
	  	//Deleting these two print statements make the program not return values???????
	  	println(Jobs.nameDictionary);
	  	println(Jobs.nameDictionary.get(Jobs.Idling));
	  	val characterID : BSONObjectID = BSONObjectID(request.session.get("id").get)
	  	val charRef = everybodyHash.getOrElse(characterID, {
	  		val record = Person.makeNew(characterID)
	  		val ref = system.actorOf(Props(classOf[Brain], record))
	  		everybodyHash+=((characterID, ref))
	  		
	  		everybody.insert(record)
	  		
	  		ref
	  	})
	  	val box = Inbox.create(system)
	  	
	  	
		val in = Iteratee.foreach((a :JsValue) => {
		  Json.fromJson[BsMessage](a) match{
		    case JsSuccess(x, _) => box.send(charRef, x)
		    case _ => box.send(box.getRef, Text("Recieved bad message: " + a.toString))
		  }
		})
		val out = Enumerator.repeat[JsValue](
		    box.receive(new FiniteDuration(5, MINUTES)) match {
		      case x : Hermes.BsMessage => {
		        Json.toJson(x)
		    }
		})
		(in, out)
  }
}