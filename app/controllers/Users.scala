package controllers

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.mvc.Controller
import play.api.mvc.WebSocket
import play.api.libs.iteratee.Concurrent
import play.api.libs.iteratee.Iteratee
import play.modules.reactivemongo.MongoController
import reactivemongo.api.collections.default.BSONCollection
import play.api.mvc.Action
import reactivemongo.bson.BSONObjectID
import models.User
import play.api.libs.json.Json
import reactivemongo.bson.BSONDocument
import com.github.t3hnar.bcrypt._
import scala.concurrent.Future
import play.api.mvc.Request
import play.api.mvc.Result
import play.api.mvc.AnyContent
import play.api.mvc.SimpleResult
import java.util.Arrays

object Users extends Controller with MongoController  {
	val collection = db[BSONCollection]("users")
	
	//TODO: This still needs to make sure the request email doesnt already exist in the system.
	def create() = Action.async(parse.json) { request =>
	  val email = request.body.\("email").toString.replace("\"", "")
	  val u = collection.find(BSONDocument("email" -> email)).one[User]
	  val password = request.body.\("password").toString.replace("\"", "").bcrypt(12)
	  val user = User(Option(BSONObjectID.generate), email, password)
	  for {
	    maybeUser <- u
	    result <- maybeUser.map { user =>
	      Future(Ok(Json.obj("status" -> "ERR")))
	    }.getOrElse(collection.insert(user).map(_ => Ok(Json.obj("status" -> "OK"))))
	  } yield result
	  //val password = request.body.\("password").toString.replace("\"", "").bcrypt(12)
	  //val user = User(Option(BSONObjectID.generate), email, password)
	  //collection.insert(user).map(_ => Ok(Json.obj("status" -> "OK")))
	}
	
	def login() = Action.async(parse.json) { request =>
	  val email = request.body.\("email").toString.replace("\"", "")
	  val password = request.body.\("password").toString.replace("\"", "")
	  val user = collection.find(BSONDocument("email" -> email)).one[User]
	  
	  for {
	    maybeUser <- user
	    result <- maybeUser.map { user =>
	      if(password.isBcrypted(user.password)) Future(Ok(Json.obj("status" -> "OK")).withSession("id" -> user.id.get.stringify, "time" -> System.currentTimeMillis().toString))
	      else Future(Unauthorized)
	    }.getOrElse(Future(Unauthorized))
	  } yield result
	}
	
	def Authenticated(f: User => Request[AnyContent] => Future[SimpleResult]) = Action.async { request =>
		def _Authenticated(f: User => Request[AnyContent] => Future[SimpleResult]) : Future[SimpleResult] = {
			val id = request.session.get("id").get
			val time = request.session.get("time").get.toLong
			val u = collection.find(BSONDocument("id" -> id)).one[User]

			for {
				maybeUser <- u
				result <- maybeUser.map { user =>
					val currentTime = System.currentTimeMillis()
					val deltaMs = 1000*60*15 //Session lives for 15 minutes
					if(currentTime - time < deltaMs) f(user)(request).map { result =>
						result.withSession("id" -> id,"time" -> System.currentTimeMillis().toString)
					}
					else Future(Redirect(routes.Application.loadAngularView("login")))
				}.getOrElse(Future(Redirect(routes.Application.loadAngularView("login"))))
			} yield result
	}
	  (request.session.get("id"), request.session.get("time")) match {
	    case (None, _) => Future(Redirect(routes.Application.loadAngularView("login")))
	    case (_, None) => Future(Redirect(routes.Application.loadAngularView("login")))
	    case (_,_) => _Authenticated(f)
	  }
	}
	
	def getCurrentUser() = Authenticated { user => request =>
	  Future(Ok(Json.toJson(user)))
	}
	
	/*def tester =  WebSocket.using[String] { request =>
	   //Concurrent.broadcast returns (Enumerator, Concurrent.Channel)
	    val (out,channel) = Concurrent.broadcast[String]
	 
	    //log the message to stdout and send response back to client
	    val in = Iteratee.foreach[String] {
	      msg => println(msg)
	            println(msg)
	             //the Enumerator returned by Concurrent.broadcast subscribes to the channel and will 
	             //receive the pushed messages
	             channel push("{\"event\": \"RESPONSE\", \"data\": \"Response data\"}")
	    }
	    (in,out)
	}*/
}
