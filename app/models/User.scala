package models

import reactivemongo.bson.BSONDocument
import reactivemongo.bson.BSONDocumentReader
import reactivemongo.bson.BSONDocumentWriter
import reactivemongo.bson.BSONObjectID
import reactivemongo.bson.BSONObjectIDIdentity
import reactivemongo.bson.BSONStringHandler
import reactivemongo.bson.Producer.nameValue2Producer
import play.api.libs.json.Json
import play.modules.reactivemongo.json.BSONFormats._

case class User(id: Option[BSONObjectID], email: String, password: String)

object User {
    implicit val userFormat = Json.format[User]
  
	implicit object UserBSONWriter extends BSONDocumentWriter[User] {
		def write(user: User): BSONDocument =
				BSONDocument(
						"_id" -> user.id.getOrElse(BSONObjectID.generate),
						"email" -> user.email,
						"password" -> user.password) 
	}

	implicit object UserBSONReader extends BSONDocumentReader[User] {
		def read(document: BSONDocument): User =
				User(
						document.getAs[BSONObjectID]("_id"), // .get? -Alex
						document.getAs[String]("email").get,
						document.getAs[String]("password").get)
	}
}