package trade

import reactivemongo.bson._
import play.api.libs.json.Json
import play.modules.reactivemongo.json.BSONFormats._
import reactivemongo.bson.DefaultBSONHandlers.BSONIntegerHandler
import play.api.libs.json.Writes
import play.api.libs.json.JsArray

object Inventory {
  val empty = Inventory(0, Vector.fill(Good.values.size)(0))
  
  implicit val JsInvWrites = new Writes[Inventory] {
    	def writes(foo: Inventory) =  { 
		  Json.obj( "gold" -> foo.gold,
		  	        "goods" -> JsArray(foo.myGoods.toSeq.map(Json.toJson(_)))
		  	        )
		}
   }
  
  
  implicit object InventoryBSONWriter extends BSONDocumentWriter[Inventory] {
    
		def write(foo: Inventory): BSONDocument ={
				var builder = BSONDocument("gold" -> foo.gold)
				for (index <- 0 until Good.values.size) builder = builder ++ BSONDocument(index.toString -> foo.myGoods.apply(index))
				builder
			}
						
	}

	implicit object InventoryBSONReader extends BSONDocumentReader[Inventory] {
		def read(document: BSONDocument): Inventory ={
				val gold = document.getAs[Int]("gold").get
				val goods = for (index <- 0 until Good.values.size) yield document.getAs[Int](index.toString).get
				Inventory(gold, goods.toVector)
				
		}
	}
}

case class Inventory (val gold : Int, myGoods : Vector[Int]) {
  assert(myGoods.length == Good.values.size)
  def update(good: Good.Goods, func : (Int => Int)) : Inventory = {
    val index = good.id
    val stuff = func.apply(myGoods.apply(index))
    Inventory(gold, myGoods.updated(index, stuff))
  }
  def updateGold(func : (Int => Int)) = Inventory(func.apply(gold), myGoods)
  
  def zip(that : Inventory) : Inventory = {
    val add = (a : (Int, Int) )=> a._1 + a._2
    Inventory( gold + that.gold, 
        that.myGoods.view.zip(this.myGoods).map(add).force.toVector
        )
  }
  lazy val valid = gold >= 0 && myGoods.forall(_>=0)
  
  def inverse = {
    Inventory (-gold, for (x <- myGoods) yield -1 * x)
  }
  def get ( good : Good.Goods) = myGoods.apply(good.id)
  
  override def toString() = "Gold: " + gold + "\nGoods: " + myGoods.toString + "\n"
}