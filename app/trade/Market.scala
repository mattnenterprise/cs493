package trade

import java.util.Comparator
import akka.actor._
import scala.collection.JavaConversions._
import scala.collection.mutable.PriorityQueue
import Hermes._
import scala.collection.immutable.HashMap
import controllers.Booter


  
  object Good extends Enumeration{
	type Goods = Value
  
	val food, supply, wood, iron, cloth, tool, gBar = Value;
  
	val goodDictionary = HashMap(
      "food" -> food,
      "supply" -> supply,
      "wood" -> wood,
      "iron" -> iron,
      "cloth" -> cloth,
      "tool" -> tool,
      "gold bar" -> gBar
    )

  }
  //A Single location where goods are traded

case class MBid( who: ActorRef, offer: Bid)

  object Market {
    import Good._
    var GAME_ON :Boolean = true
    val allTraders :Array[ActorRef]= (for (good <- Good.values) yield Booter.system.actorOf(Props(classOf[Trader], good))).toArray
    
    class Trader(good: Goods)extends Actor{
      
      var buyers = new PriorityQueue[MBid]()(Ordering.by(_.offer.rate))
      var sellers = new PriorityQueue[MBid]()(Ordering.by(_.offer.rate * -1));
      
      def receive =  {
        case x: Bid if x.what == good => 
          { if (x.isBuying) buyers.enqueue( MBid (sender, x))
        	else sellers.enqueue(MBid(sender, x));
        	trade()
          }
        case Cancel(x, who) if x.what == good => 
          {
            val mb = MBid(sender, x);
            val success = if(x.isBuying) {
              buyers = buyers.filter(b => b != mb)
            } else {
              sellers = sellers.filter(b => b != mb);
            }
            sender ! Ok(Cancel(x, who))
          }
        case Reject(_) | Ok(_) | Failed(_) => Unit //Is there a good place to log this?
        case x : Message => sender ! Reject(x)
      }
      
      
      
      def trade() :Unit = {
        if ( !buyers.isEmpty && ! sellers.isEmpty && buyers.head.offer.rate >= sellers.head.offer.rate){
          val MBid(bwho, buyer) = buyers.dequeue
          val MBid(swho, seller) = sellers.dequeue
          val amt = Math.min(buyer.howMany, seller.howMany)
          val price : Int = buyer.rate + ((buyer.rate-seller.rate) / 2) //average without overflow
          
          bwho ! Deal(buyer, amt, price)
          swho ! Deal(seller, amt, price);
          trade()
        }
      }
      
    }
    
 }


