angular.module("app")
	.controller("GameCtrl", function($scope, $http, $location, $websocket, $dialogs) {
		$scope.getCurrentUser = function() {
			$http.get('/api/user/user')
				.success(function(data, status, headers, config) {
					$scope.email = {address: data.email};
				})
				.error(function(data, status, headers, config) {
					$location.path('/login');
				});
		};
		
		$scope.getCurrentUser();
		
		$scope.inventory = [
		   {name:"food", value: 0},
		   {name:"supply", value: 0},
		   {name:"wood", value: 0},
		   {name:"iron", value: 0},
		   {name:"cloth", value: 0},
		   {name:"tool", value: 0},
		   {name:"gold bar", value: 0}
		];
		$scope.gold = 0;
		$scope.prestige = 0;
		$scope.job = '';
		$scope.trades = [];
		$scope.textScroller = { data: 'Welcome to the game!\n' };
		
		 var ws = $websocket.$new({
			 url: 'ws://www.learncs.us/talk',
			 reconnect: true
		 });
		 
		  ws.$on('$open', function () {
			  ws.$emit('Refresh', '');
		  })
		  .$on('Body', function (message) {
		    console.log('something incoming from the server: ' + JSON.stringify(message));
		    $scope.gold = message.inv.gold;
		    $scope.prestige = message.prestige;
		    $scope.trades = [];
		    if(typeof(message.job.offers) !== 'undefined') {
		    	$scope.job = 'bartering';
		    	var numOffers = message.job.offers.length;
		    	for(var i = 0;i < numOffers; i++) {
		    		var offer = message.job.offers[i];
		    		var wh = 'food';
		    		if(offer.what === 0) {
			        	  wh = 'food';
			          } else if(offer.what === 1) {
			        	  wh = 'supply';
			          } else if(offer.what === 2) {
			        	  wh = 'wood';
			          } else if(offer.what === 3) {
			        	  wh = 'iron';
			          } else if(offer.what === 4) {
			        	  wh = 'cloth';
			          } else if(offer.what === 5) {
			        	  wh = 'tool';
			          } else if(offer.what === 6) {
			        	  wh = 'gold bar';
			          }
		    		var tradeOption = offer.isBuying ? 'buying' : 'selling';
		    		$scope.trades.push({ buyingOrSelling: tradeOption, what: wh, howMany: offer.howMany, price: offer.rate });
		    		$scope.$apply();
		    	}
		    }
		    else {
		    	$scope.job = message.job.name;
		    }
		    
		    var numInv = message.inv.goods.length;
		    for (var i = 0; i < numInv; i++) {
		        $scope.inventory[i].value = message.inv.goods[i];
		    }
		    $scope.$apply();
		  })
		  .$on('Text', function (message) {
			console.log('some text coming from the server: ' + message);
			$scope.textScroller.data += message + '\n';
			$scope.$apply();
		  });
		  
		  ws.$on('$close', function () {
	            console.log('Noooooooooou, I want to have more fun with ngWebsocket, damn it!');
	      });
		  
		  function refresh() {
				ws.$emit('Refresh', '');
			    setTimeout(refresh, 5000);
			}

		  refresh();
		  
		  $scope.changeJob = function() {
				var dlg = null;
				dlg = $dialogs.create('views/changejob', 'changeJob', {}, {key: false,back: 'static'});
				dlg.result.then(function(newJob){
			          $scope.job = newJob;
			          ws.$emit('ChangeJob', $scope.job);
			          var numTrades = $scope.trades.length;
			          for(var i = 0;i < numTrades; i++) {
			        	  var trade = $scope.trades[i];
			        	  var isBuying = trade.buyingOrSelling === 'buying' ? true : false;
			        	  ws.$emit('Cancel', '{isBuying: ' + isBuying + ', what: ' + trade.what + ', howMany: ' + trade.howMany + ', rate: ' + trade.price + '}');
			          }
			          $scope.trades = [];
			        },function(){
			          $scope.job = 'idle';
			          ws.$emit('ChangeJob', $scope.job);
			        });
			};
			
			
			$scope.removeTrade = function(i) {
				var isBuying = $scope.trades[i].buyingOrSelling === 'buying' ? true : false;
				var whatData = 0;
		          if($scope.trades[i].what === 'food') {
		        	  whatData = 0;
		          } else if($scope.trades[i].what === 'supply') {
		        	  whatData = 1;
		          } else if($scope.trades[i].what === 'wood') {
		        	  whatData = 2;
		          } else if($scope.trades[i].what === 'iron') {
		        	  whatData = 3;
		          } else if($scope.trades[i].what === 'cloth') {
		        	  whatData = 4;
		          } else if($scope.trades[i].what === 'tool') {
		        	  whatData = 5;
		          } else if($scope.trades[i].what === 'gold bar') {
		        	  whatData = 6;
		          }
				ws.$emit('Cancel', {isBuying: isBuying, what: whatData, howMany: $scope.trades[i].howMany, rate: $scope.trades[i].price })
				console.log('{isBuying: ' + isBuying + ', what: ' + $scope.trades[i].what + ', howMany: ' + $scope.trades[i].howMany + ', rate: ' + $scope.trades[i].price + '}');
				$scope.trades.splice(i, 1);
				refresh();
			}
			
		 $scope.makeTrade = function() {
			 var dlg = null;
				dlg = $dialogs.create('views/maketrade', 'makeTrade', {}, {key: false,back: 'static'});
				dlg.result.then(function(trade){
			          var isBuying = trade.buyingOrSelling === 'buying' ? true : false;
			          var whatData = 0;
			          if(trade.what === 'food') {
			        	  whatData = 0;
			          } else if(trade.what=== 'supply') {
			        	  whatData = 1;
			          } else if(trade.what === 'wood') {
			        	  whatData = 2;
			          } else if(trade.what === 'iron') {
			        	  whatData = 3;
			          } else if(trade.what === 'cloth') {
			        	  whatData = 4;
			          } else if(trade.what === 'tool') {
			        	  whatData = 5;
			          } else if(trade.what === 'gold bar') {
			        	  whatData = 6;
			          }
			          ws.$emit('Bid',{isBuying: isBuying, what: whatData, howMany: trade.howMany, rate: trade.price });
			          $scope.job = 'bartering';
			          //$scope.$apply();
			        },function(){
			          //Do nothing
			        });
		 };
	})
	.controller('changeJob',function($scope,$modalInstance,data){
		$scope.jobs = [
		                 'idle',
		                 'farm',
	    			     'supply',
	    				 'heard',
	    				 'forest',
	    				 'smith',
	   					 'mine',
	    				 'pan',
	    				 'mint'
		               ];
		$scope.newJob = {name: 'idle'};
		
		$scope.cancel = function(){
		    $modalInstance.dismiss('canceled');  
		}; // end cancel
		
		$scope.save = function() {
		    $modalInstance.close($scope.newJob.name);
		}; // end save
	}).controller('makeTrade', function($scope, $modalInstance, data) {
		
		$scope.tradeItems = ["food", "supply", "wood", "iron", "cloth", "tool", "gold bar"];
		$scope.tradeOptions = ['buying', 'selling'];
		$scope.trade = {what: 'food', howMany: 0, price: 0, buyingOrSelling: 'buying'};
		
		$scope.cancel = function(){
		    $modalInstance.dismiss('canceled');  
		}; // end cancel
		
		$scope.save = function() {
		    $modalInstance.close($scope.trade);
		}; // end save*/
	});